package com.android.test.models.users;

public class LocationModel {

    private String city;
    private String state;
    private String country;
    private String postcode;
    private StreetModel street;
    private CoordonatesModel coordinates;
    private TimezoneModel timezone;

    public LocationModel() {
        this.city = "";
        this.state = "";
        this.country = "";
        this.postcode = "";
        this.street = new StreetModel();
        this.coordinates = new CoordonatesModel();
        this.timezone = new TimezoneModel();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public StreetModel getStreet() {
        return street;
    }

    public void setStreet(StreetModel street) {
        this.street = street;
    }

    public CoordonatesModel getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordonatesModel coordinates) {
        this.coordinates = coordinates;
    }

    public TimezoneModel getTimezone() {
        return timezone;
    }

    public void setTimezone(TimezoneModel timezone) {
        this.timezone = timezone;
    }
}
