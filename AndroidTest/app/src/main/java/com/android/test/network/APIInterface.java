package com.android.test.network;

import static com.android.test.network.URLConstants.BASE_URL;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET(BASE_URL)
    Call<String> getUsers(@Query(ParamConstants.PAGE) int page,
                         @Query(ParamConstants.RESULTS) int results,
                         @Query(ParamConstants.SEED) String seed);
}

