package com.android.test.models.users;

public class TimezoneModel {

    private String offset;
    private String description;

    public TimezoneModel() {
        this.offset = "";
        this.description = "";
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
