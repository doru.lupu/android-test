package com.android.test.models.info;

public class InfoModel {

    private String seed;
    private int results;
    private int page;
    private String version;

    public InfoModel() {
        this.seed = "";
        this.results = 0;
        this.page = 0;
        this.version = "";
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public int getResult() {
        return results;
    }

    public void setResult(int result) {
        this.results = result;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
