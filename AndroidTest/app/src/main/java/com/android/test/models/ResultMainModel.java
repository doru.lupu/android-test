package com.android.test.models;

import com.android.test.models.info.InfoModel;
import com.android.test.models.users.UserModel;

import java.util.ArrayList;

public class ResultMainModel {

    private ArrayList<UserModel> results;
    private InfoModel info;

    public ResultMainModel() {
        this.results = new ArrayList<>();
        this.info = new InfoModel();
    }

    public ArrayList<UserModel> getResults() {
        return results;
    }

    public void setResults(ArrayList<UserModel> results) {
        this.results = results;
    }

    public InfoModel getInfo() {
        return info;
    }

    public void setInfo(InfoModel info) {
        this.info = info;
    }
}
