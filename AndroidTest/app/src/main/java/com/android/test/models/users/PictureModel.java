package com.android.test.models.users;

public class PictureModel {

    private String large;
    private String thumbnail;
    private String medium;

    public PictureModel() {
        this.large = "";
        this.thumbnail = "";
        this.medium = "";
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }
}
