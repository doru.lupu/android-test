package com.android.test.models.users;

public class RegisterModel {

    private String date;
    private int age;

    public RegisterModel() {
        this.date = "";
        this.age = 0;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
