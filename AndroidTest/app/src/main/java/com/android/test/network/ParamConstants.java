package com.android.test.network;

public class ParamConstants {

    public static final String PAGE = "page";
    public static final String RESULTS = "results";
    public static final String SEED = "seed";
}
