package com.android.test.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.test.R;
import com.android.test.adapters.UserAdapter;
import com.android.test.databinding.FragmentUsersBinding;
import com.android.test.models.ResultMainModel;
import com.android.test.models.users.UserModel;
import com.android.test.network.APIClient;
import com.android.test.network.APIInterface;
import com.android.test.utils.CustomLoadingDialog;
import com.android.test.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersFragment extends Fragment {

    private FragmentUsersBinding binding;
    private UserAdapter mUserAdapter;
    private ArrayList<UserModel> mUsersData;
    private static final String TAG = Utils.makeTAG(UsersFragment.class);
    private int pageIndex = 1;
    boolean isCallInProgress = false;

    public UsersFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentUsersBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null) {
            getActivity().setTitle(getResources().getString(R.string.title_users));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (mUsersData == null) mUsersData = new ArrayList<>();

        mUserAdapter = new UserAdapter(getContext(), mUsersData);
        initListAndScroll();
        getUsers();
    }

    private void getUsers() {
        CustomLoadingDialog.showLoadingDialog(getActivity());
        isCallInProgress = true;
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> newsCall = apiInterface.getUsers(pageIndex, 20, "abc");
        newsCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                CustomLoadingDialog.hideLoadingDialog(getActivity());
                isCallInProgress = false;
                Gson gson = new Gson();
                ResultMainModel responseModel = gson.fromJson(response.body(), ResultMainModel.class);
                if (responseModel != null) {
                    mUsersData.addAll(responseModel.getResults());
                    pageIndex++;
                    if (getActivity() != null && isAdded()) {
                        getActivity().runOnUiThread(() -> updateUI(responseModel.getInfo().getResult()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                CustomLoadingDialog.hideLoadingDialog(getActivity());
                isCallInProgress = false;
                Log.e(TAG, t.toString());
            }
        });
    }

    private void updateUI(int range) {
        mUserAdapter.notifyItemRangeInserted(mUsersData.size(), mUsersData.size() + range);
    }

    private void initListAndScroll() {
        RecyclerView usersList = binding.rvListUsers;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL,
                false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(usersList.getContext(),
                linearLayoutManager.getOrientation());
        usersList.addItemDecoration(dividerItemDecoration);
        usersList.setLayoutManager(linearLayoutManager);

        usersList.setAdapter(mUserAdapter);

        usersList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= mUsersData.size() - 4
                        && pageIndex < 4 && !isCallInProgress) {
                    getUsers();
                }
            }
        });
    }
}
