package com.android.test.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.content.ContextCompat;

import com.android.test.R;

public class CustomLoadingDialog {

    private static Dialog mLoadingDialog;

    public static void showLoadingDialog(Activity activity) {
        int color = ContextCompat.getColor(activity, R.color.color_black);

        mLoadingDialog = new Dialog(activity, android.R.style.Theme_Translucent);
        mLoadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.setCanceledOnTouchOutside(false);

        Window window = mLoadingDialog.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        window.setNavigationBarColor(color);
        mLoadingDialog.setContentView(R.layout.dialog_custom_loading);

        mLoadingDialog.setOnKeyListener((dialog, keyCode, event) -> {
            if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                activity.onBackPressed();
                return true;
            }
            return false;
        });

        activity.runOnUiThread(() -> mLoadingDialog.show());
    }

    public static boolean isVisible() {
        return mLoadingDialog != null && mLoadingDialog.isShowing();
    }

    public static void hideLoadingDialog(Activity activity) {
        try {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                activity.runOnUiThread(() -> mLoadingDialog.dismiss());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

