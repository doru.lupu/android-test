package com.android.test.models.users;

public class DOBModel {

    private int age;
    private String date;

    public DOBModel() {
        this.age = 0;
        this.date = "";
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
