package com.android.test.models.users;

import static com.android.test.utils.Utils.EMPTY_STRING;

public class UserModel {

    private String gender;
    private String email;
    private NameModel name;
    private LocationModel location;
    private LoginModel login;
    private DOBModel dob;
    private RegisterModel registered;
    private IdModel id;
    private PictureModel picture;
    private String phone;
    private String cell;
    private String nat;

    public UserModel() {
        this.gender = "";
        this.email = "";
        this.name = new NameModel();
        this.location = new LocationModel();
        this.login = new LoginModel();
        this.dob = new DOBModel();
        this.registered = new RegisterModel();
        this.id = new IdModel();
        this.picture = new PictureModel();
        this.phone = "";
        this.cell = "";
        this.nat = "";
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public NameModel getName() {
        return name;
    }

    public void setName(NameModel name) {
        this.name = name;
    }

    public LocationModel getLocation() {
        return location;
    }

    public void setLocation(LocationModel location) {
        this.location = location;
    }

    public LoginModel getLogin() {
        return login;
    }

    public void setLogin(LoginModel login) {
        this.login = login;
    }

    public DOBModel getDob() {
        return dob;
    }

    public void setDob(DOBModel dob) {
        this.dob = dob;
    }

    public RegisterModel getRegister() {
        return registered;
    }

    public void setRegister(RegisterModel register) {
        this.registered = register;
    }

    public IdModel getId() {
        return id;
    }

    public void setId(IdModel id) {
        this.id = id;
    }

    public PictureModel getPicture() {
        return picture;
    }

    public void setPicture(PictureModel picture) {
        this.picture = picture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getNat() {
        return nat;
    }

    public void setNat(String nat) {
        this.nat = nat;
    }

    public String getFullName() {
        return (getName() == null) ? EMPTY_STRING : getName().getFirst() + " " + getName().getLast();
    }
}
