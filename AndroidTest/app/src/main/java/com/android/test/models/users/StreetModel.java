package com.android.test.models.users;

public class StreetModel {

    private int number;
    private String name;

    public StreetModel() {
        this.number = 0;
        this.name = "";
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
