package com.android.test.models.users;

public class IdModel {

    private String name;
    private String  value;

    public IdModel() {
        this.name = "";
        this.value = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
