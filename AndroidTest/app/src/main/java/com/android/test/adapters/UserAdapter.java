package com.android.test.adapters;

import static com.android.test.utils.Utils.EMPTY_STRING;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.test.R;
import com.android.test.databinding.ItemUserListBinding;
import com.android.test.models.users.UserModel;
import com.android.test.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private final ArrayList<UserModel> mData;
    private final Context mContext;

    public UserAdapter(Context context, ArrayList<UserModel> items) {
        this.mData = items;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserListBinding binding = ItemUserListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new UserViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        UserModel userModel = mData.get(position);

        if (!TextUtils.isEmpty(userModel.getFullName())) {
            holder.mFullName.setText(userModel.getFullName());
        } else {
            holder.mFullName.setText(EMPTY_STRING);
        }

        if (userModel.getDob() != null && userModel.getDob().getAge() > 0) {
            holder.mAgeLocation.setText(mContext.getResources()
                    .getString(R.string.user_item_list_age_location,
                            userModel.getDob().getAge(),
                            userModel.getNat()));
        } else {
            holder.mAgeLocation.setText(EMPTY_STRING);
        }

        if (userModel.getPicture() != null && !TextUtils.isEmpty(userModel.getPicture().getMedium())) {
            Picasso.get()
                    .load(userModel.getPicture().getMedium())
                    .transform(new CropCircleTransformation())
                    .into(holder.mLogo);
        } else {
            holder.mLogo.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_users));
        }

        //TODO I don't know exactly what to display for the date and attachment icon
        if (position % 2 == 0) {
            holder.mAttachment.setVisibility(View.VISIBLE);
        } else {
            holder.mAttachment.setVisibility(View.GONE);
        }

        if(userModel.getRegister() != null && !TextUtils.isEmpty(userModel.getRegister().getDate())) {
            holder.mOnlineDate.setText(Utils.getHourTimeFromDate(userModel.getRegister().getDate()));
        }
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        private final TextView mFullName, mAgeLocation, mOnlineDate;
        private final ImageView mLogo, mAttachment, mFavorite;

        UserViewHolder(ItemUserListBinding itemBinding) {
            super(itemBinding.getRoot());
            mFullName = itemBinding.tvUserFullName;
            mAgeLocation = itemBinding.tvUserAgeLocation;
            mOnlineDate = itemBinding.tvLastOnline;
            mLogo = itemBinding.ivUserLogo;
            mAttachment = itemBinding.ivAttachment;
            mFavorite = itemBinding.ivFavorite;
        }
    }
}


