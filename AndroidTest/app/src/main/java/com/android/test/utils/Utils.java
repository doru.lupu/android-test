package com.android.test.utils;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class Utils {

    public static final String EMPTY_STRING = "";
    public static final String TAG_BEGIN = "ANDROIDTEST_";
    private static final String TAG = makeTAG(Utils.class);

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat sHourTimeFormat = new SimpleDateFormat("HH:mm");

    public static String makeTAG(Class fle) {
        String str = TAG_BEGIN + fle.getSimpleName();
        if (str.length() > 23) {
            str = str.substring(0, 23);
        }
        return str;
    }

    public static String getHourTimeFromDate(String dateString) {
        Date initialDate = null;
        try {
            initialDate = sDateFormat.parse(dateString);
            return sHourTimeFormat.format(Objects.requireNonNull(initialDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return EMPTY_STRING;
        }
    }
}

